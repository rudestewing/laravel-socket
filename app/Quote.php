<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'content'
    ];

    public function comments()
    {
        return $this->hasMany('App\Comment','post_id','id');
    }

}
