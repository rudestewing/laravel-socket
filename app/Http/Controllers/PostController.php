<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function index()
    {
        $data = Post::all();
        return view('posts.index',[
            'posts' => $data
        ]);
    }

    public function save(Request $req)
    {
        $post = new Post([
            'title' => $req->title,
            'content' => $req->content
        ]);
        $post->save();
        return redirect()->route('posts');
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show',[
            'post' => $post
        ]);
    }

}
