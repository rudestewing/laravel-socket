<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quote;
use App\Comment;
use App\Events\NewComment;

class CommentController extends Controller
{
    public function save(Request $req,$id)
    {
        $comment = new Comment([
            'body' => $req->body,
            'post_id' => $id
        ]);
        $comment->save();

        $data = [
            'id' => $comment->id,
            'body' => $comment->body,
            'post_id' => $comment->post_id
        ];

        broadcast(new NewComment($data));
        
        return redirect()->route('post.show',['id'=>$id]);
        
    }

}
