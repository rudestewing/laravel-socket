<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quote;


class QuoteController extends Controller
{
    public function index()
    {
        $data = Quote::all();

        return view('quotes',[
            'quotes' => $data
        ]);
    }

    public function save(Request $req)
    {
        $quote = new Quote([
            'content' => $req->content
        ]);
        $quote->save();

        $data = [
            'id' => $quote->id,
            'content' => $quote->content
        ];
        
        return redirect()->route('quotes');
    }

    public function show($id)
    {
        $data = Quote::find($id);
        return view('show-quote',[
            'quote' => $data
        ]);
    }
}
