@extends('layouts.app');

@section('content')
<div class="container">
    <div class="container">
        <h3>Post</h3>
        {{$post->content}}
    </div>
    <hr>
    <form action="{{route('comment.save',['id'=>$post->id])}}" method="post">
        <div class="form-group col-md-4">
            <label for="post">comment</label>
            <input type="text" name="body" class="form-control">
        </div>
        <div class="form-group col-md-4">
            <button type="submit" class="btn btn-sm btn-default">submit comment</button>
        </div>
        {{csrf_field()}}
        {{method_field('put')}}
    </form>
    <hr>
    <ul id="comment">
        @foreach($post->comments as $comment)
            <li>{{$comment->body}}</li>
        @endforeach
    </ul>
</div>
@stop

@section('scripts')
    <script src="{{url('/')}}:6001/socket.io/socket.io.js"></script>
    
    <script>
        var post_id = '{!! $post->id !!}';
        
        Echo
            .channel('post.' + post_id)
            .listen('NewComment',function(e){
                $('#comment').prepend(
                    '<li>' +e.body+ '</li>'
                );
            });

    </script>
@stop
