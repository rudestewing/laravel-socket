@extends('layouts.app');

@section('content')
<div class="container">
    <form action="{{route('post.save')}}" method="post">
        <div class="form-group col-md-4">
            <label for="post">title</label>
            <input type="text" name="title" class="form-control">
        </div>
        <div class="form-group col-md-4">
            <label for="post">content</label>
            <input type="text" name="content" class="form-control">
        </div>
        <div class="form-group col-md-4">
            <button type="submit" class="btn btn-sm btn-default">save post</button>
        </div>
        {{csrf_field()}}
    </form>
    <hr>

    <ul>
        @foreach($posts as $post)
            <li><a href="{{route('post.show',['id'=>$post->id])}}">{{$post->content}}</a></li>
        @endforeach
    </ul>
</div>

@stop
