<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/posts','PostController@index')->name('posts');
Route::post('/post','PostController@save')->name('post.save');
Route::get('/post/{id}','PostController@show')->name('post.show');

Route::put('/post/{id}/comment','CommentController@save')->name('comment.save');